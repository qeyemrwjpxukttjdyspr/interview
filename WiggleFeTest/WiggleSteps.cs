﻿using System;
using System.Threading;
using FluentAssertions;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using TechTalk.SpecFlow;

namespace WiggleFeTest
{
    [Binding]
    internal sealed class WiggleSteps
    {
        public static ChromeDriver _driver;
        public string product;


        [Given(@"I have navigated to the wiggle website")]
        public void GivenIHaveNavigatedToTheWiggleWebsite()
        {
            _driver = new ChromeDriver();
            _driver.Navigate().GoToUrl("https://www.wiggle.co.uk/");
            Thread.Sleep(2000);
        }

        [Given(@"the wiggle website is displayed")]
        public void GivenTheWiggleWebsiteIsDisplayed()
        {
            var element = _driver.FindElement(By.ClassName("bem-header__logo"));
            Assert.IsNotNull(element);
            if (element == null)
            {
                throw new Exception("Not on wiggle page");
            }
        }

        [When(@"I navigate to the wiggle website")]
        public void WhenINavigateToTheWiggleWebsite()
        {
            _driver.Navigate().GoToUrl("https://www.wiggle.co.uk/");
            Thread.Sleep(2000);
        }

        [When(@"I have searched for an item")]
        public void WhenIHaveSearchedForAnItem()
        {
            _driver.FindElement(By.Name("s")).SendKeys("head torch");
            Thread.Sleep(100);
            _driver.FindElement(By.ClassName("icon-search")).Click();
            Thread.Sleep(3000);
            var results = _driver.FindElements(By.ClassName("bem-product-thumb__name--grid"));
            var total = results.Count;
            for (int i = 0; i < total; i++)
            {
                if (!results[i].Enabled) continue;
                results[i].Click();
                Thread.Sleep(2000);
                break;
            }
        }

        [Then(@"I add the item to my basket")]
        public void ThenIAddTheItemToMyBasket()
        {
            product = _driver.FindElement(By.Id("productTitle")).Text;
            _driver.FindElement(By.Id("quickBuyButton")).Click();
            Thread.Sleep(500);
        }

        [When(@"I navigate to my basket")]
        public void WhenINavigateToMyBasket()
        {
            Thread.Sleep(500);
            _driver.Navigate().GoToUrl("https://www.wiggle.co.uk/basket");
            Thread.Sleep(500);
        }

        [Then(@"the item I have added is shown")]
        public void ThenTheItemIHaveAddedIsShown()
        {
            var expected = _driver.FindElement(By.ClassName("item-name")).Text;
            expected.Should().Contain(product);
        }

        [AfterFeature]
        public static void After()
        {
            _driver.Dispose();
        }
    }
}
