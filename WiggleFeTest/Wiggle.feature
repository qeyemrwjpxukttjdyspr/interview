﻿Feature: Wiggle Automation

Scenario: 01 - Navigate to the Wiggle Website
	Given I have navigated to the wiggle website
	And the wiggle website is displayed

Scenario: 02 - Add Item to basket
	When I navigate to the wiggle website
	And I have searched for an item
	Then I add the item to my basket
	When I navigate to my basket 
	Then the item I have added is shown

